# Arch Linux Toolbox

Arch Linux Toolbox image. This image is meant to be used with the `toolbox` command.

## Installation

```
toolbox create -i registry.gitlab.com/theevilskeleton/archlinux-toolbox
```

## Remove

```
toolbox rmi -f archlinux-toolbox
```
